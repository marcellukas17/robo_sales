# Robo_Sales
Meu Robo_Sales ele fica em loop dando voltas, e depois muda para andar 50 ou 100 pixels para frente e gira para direita 45 ou 90 pixels, girando a arma 180 graus para a esquerda. Já as voltas são 50 pixels para frente e vira para a direita 45 pixels, girando a arma 360 graus.
## Os pontos fortes do Robo_Sales são:
- Possui duas combinações de movimento.
- Consegue esquivar dos tiros e ao mesmo tempo atirar.
- Possui direção reversa.
## Os pontos fracos do Robo_Sales são:
- Mesmo com a direção reversa fica travado na borda às vezes.
- Quando está finalizando o robo adversário pode mudar de movimento repentinamente.

Fiquei feliz em participar e ter adquirido uma experiência de como funciona o desenvolvimento de jogos.